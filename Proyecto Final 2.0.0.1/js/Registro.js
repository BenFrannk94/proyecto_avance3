document.getElementById('aviso1').style.display="none";
document.getElementById('aviso2').style.display="none";
document.getElementById('aviso3').style.display="none";
document.getElementById('aviso4').style.display="none";
document.getElementById('aviso5').style.display="none";
document.getElementById('aviso6').style.display="none";
const registro = document.getElementById("registro");
const inputs=document.querySelectorAll("#registro input");
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/,
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, 
	password: /^.{4,12}$/ 
}
let condicion={
    usuario:false,
    pass:false,
    tipos:false,
    facultad:false,
    nacimiento:false
}
const validar=(e)=>{
    switch(e.target.name){
        case "usuario":
            validarcampos(expresiones.usuario,e.target.value,'aviso1','usuario');
        break;
        case "pass":
            validarcampos(expresiones.password,e.target.value,'aviso2','pass');
        break;
        case "tipos":
            if(e.target.value!=""){
                document.getElementById('aviso3').style.display="none"
                condicion['tipos']=true;
               }else{
                document.getElementById('aviso3').style.display="block"
                document.getElementById('aviso3').style.color="red";
                condicion['tipos']=false;
               }
        break;
        case "facultad":
            validarcampos(expresiones.nombre,e.target.value,'aviso4','facultad');
        break;
        case "nacimiento":
            validarcampos(expresiones.nombre,e.target.value,'aviso5','nacimiento');
        break;
    }
}
const validarcampos=(expresion, input, campo,comproba)=>{
    if(expresion.test(input)){
        document.getElementById(campo).style.display="none";
        condicion[comproba]=true;
       }else{
        document.getElementById(campo).style.display="block";
        document.getElementById(campo).style.color="red";
        condicion[comproba]=false;
       }
}
inputs.forEach((input)=>{
    input.addEventListener('keyup',validar);
    input.addEventListener('blur',validar);
});
registro.addEventListener('submit',(e)=>{
    
    if(condicion.usuario && condicion.pass && condicion.tipos && condicion.facultad && condicion.nacimiento ){
        document.getElementById('aviso6').style.display="none"
    }else{
        document.getElementById('aviso6').style.display="block"
        document.getElementById('aviso6').style.color="red";
        e.preventDefault();
    }
})

