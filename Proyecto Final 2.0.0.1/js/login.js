document.getElementById('aviso1').style.display="none";
document.getElementById('aviso2').style.display="none";
document.getElementById('aviso3').style.display="none";
const login = document.getElementById("login");
const inputs=document.querySelectorAll("#login input");
const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-]{4,16}$/, 
	password: /^.{4,12}$/ 
}
let condicion={
    usuario:false,
    pass:false,
}
const validar=(e)=>{
    switch(e.target.name){
        case "usuario":
            validarcampos(expresiones.usuario,e.target.value,'aviso1','usuario');
        break;
        case "pass":
            validarcampos(expresiones.password,e.target.value,'aviso2','pass');
        break;
    }
}
const validarcampos=(expresion, input, campo,comproba)=>{
    if(expresion.test(input)){
        document.getElementById(campo).style.display="none";
        condicion[comproba]=true;
       }else{
        document.getElementById(campo).style.display="block";
        document.getElementById(campo).style.color="red";
        condicion[comproba]=false;
       }
}
inputs.forEach((input)=>{
    input.addEventListener('keyup',validar);
    input.addEventListener('blur',validar);
});
login.addEventListener('submit',(e)=>{
    
    if(condicion.usuario && condicion.pass){
        document.getElementById('aviso3').style.display="none"
    }else{
        document.getElementById('aviso3').style.display="block"
        document.getElementById('aviso3').style.color="red";
        e.preventDefault();
    }
})